const CURRENCIES = {}

const fetchJSON = (url) => {
  return fetch(url).then((response) => response.json())
};

const setCurrencies = (data) => {
  Object.entries(data).forEach(([code, symbol]) => {
    CURRENCIES[code] = { symbol };
  });
  fillCurrencies(document.getElementById('c1'), data);
  fillCurrencies(document.getElementById('c2'), data);
}

const fillCurrencies = (select, data) => {
  select.innerHTML = Object.entries(data).map(([code, symbol]) => {
    return `<option value='${code}'>${symbol}</option>`;
  }).join("\n");
}

const setCurrencyNames = (data) => {
  Object.entries(data).forEach(([code, name]) => {
    CURRENCIES[code].name = name
  });
}

const setRates = (data) => {
  Object.entries(data).forEach(([code, rate]) => {
    CURRENCIES[code].rate = rate
  });
}

addEventListener('load', (e) => {
  const LOCALE = 'en';

  fetchJSON('currencies.json').then(setCurrencies);
  fetchJSON(`currencies.${LOCALE}.json`).then(setCurrencyNames);
  fetchJSON('rates.json').then(setRates);
  console.log('Currencies: ', CURRENCIES);
  addListeners();
});

const addListeners = () => {
  document.getElementById('c1').addEventListener('change', update);
  document.getElementById('c2').addEventListener('change', update);
  document.getElementById('amt1').addEventListener('input', update);
  document.getElementById('amt2').addEventListener('input', update);
};

const update = (event) => {
  let source, target;
  if (event.target.id === 'amt1' || event.target.id === 'c2') {
    source = document.getElementById('amt1');
    target = document.getElementById('amt2');
    sourceCurrency = document.getElementById('c1').value;
    targetCurrency = document.getElementById('c2').value;
  } else if (event.target.id === 'amt2' || event.target.id === 'c1') {
    source = document.getElementById('amt2');
    target = document.getElementById('amt1');
    sourceCurrency = document.getElementById('c2').value;
    targetCurrency = document.getElementById('c1').value;
  } else {
    throw new Error(`unexpected target ${event.target.id}`);
  }
  sourceValue = Number(source.value);
  targetValue = sourceValue /
    CURRENCIES[sourceCurrency].rate *
    CURRENCIES[targetCurrency].rate
  target.value = targetValue.toFixed(2);
}
